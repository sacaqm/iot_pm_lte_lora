# IOT PM 
Zephyr project to read particulate matter concentration in air and store it in a database through LTE

## Project contents

- boards: contains an overlay file to enable the i2c2 interface on pins 30 (SCL) and 31 (SDA) of the nrf9160DK
- sen5x: header files to control the SEN50 or a SEN55 over I2C
- lte_client: header files to control the LTE, the GPS and HTTP requests
- src: source code 
- prj.conf: Zephyr project config to enable the I2C and the PINCONTROL 
- CMakeLists.txt 

## How to compile this project

0. Setup the environment 
```
source sacaqmsw/setup.sh
```

1. Change to the iotpm folder
``` 
cd sacaqmsw/iotpm 
```

2. Make a build directory
```
mkdir build
```

3. Change to the build directory
```
cd build
```

4. Config the release for building
```
cmake ..
```

5. Compile the software
```
make 
```

6. Flash to the board 
```
make flash
```



