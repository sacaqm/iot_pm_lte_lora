#include <zephyr/kernel.h>
#include <lte_client/lte_client.h>
#include <lora_client/lora_client.h>
#include <zephyr/drivers/gpio.h>
#include <sen5x/sen5x.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void build_request(char * str, struct sen5x_measurement * pmm, struct lte_client_net_info * net, struct lte_client_gps_info * gps){
	str += sprintf(str, "cmd=add_sen55");
	str += sprintf(str, "&sensor_id=%15s",net->imei);
	str += sprintf(str, "&area=%s",net->area);
	str += sprintf(str, "&operator=%s",net->operator);
	str += sprintf(str, "&cellid=%s",net->cellid);
	if(gps->valid){
		str += sprintf(str, "&lat=%s",gps->latitude_string);
		str += sprintf(str, "&lon=%s",gps->longitude_string);
		str += sprintf(str, "&alt=%s",gps->altitude_string);
	}
	str += sprintf(str, "&temp=%i.%02i",(pmm->ambient_temperature / 200),abs(pmm->ambient_temperature % 200)>>1);
	str += sprintf(str, "&humi=%i.%02i",(pmm->ambient_humidity / 100),(pmm->ambient_humidity % 100));
	str += sprintf(str, "&pm1p0=%i.%02i",(pmm->mass_concentration_pm1p0 / 10),(pmm->mass_concentration_pm1p0 % 10));
	str += sprintf(str, "&pm2p5=%i.%02i",(pmm->mass_concentration_pm2p5 / 10),(pmm->mass_concentration_pm2p5 % 10));
	str += sprintf(str, "&pm4p0=%i.%02i",(pmm->mass_concentration_pm4p0 / 10),(pmm->mass_concentration_pm4p0 % 10));
	str += sprintf(str, "&pm10p0=%i.%02i",(pmm->mass_concentration_pm10p0 / 10),(pmm->mass_concentration_pm10p0 % 10));
	str += sprintf(str, "&voc=%i.%02i",(pmm->voc_index / 10),abs(pmm->voc_index % 10));
	str += sprintf(str, "&nox=%i.%02i",(pmm->nox_index / 10),abs(pmm->nox_index % 10));
}

void main(void)
{
	
	printk("Welcome to IOT_PM_LTE_LORA running on %s\n", CONFIG_BOARD);
	printk("Wait 5 seconds...\n");
	k_msleep(5000);
	
	static const struct gpio_dt_spec button1 = GPIO_DT_SPEC_GET(DT_ALIAS(sw2), gpios);
	if (!device_is_ready(button1.port)) {return;}
	gpio_pin_configure_dt(&button1, GPIO_INPUT);
	bool slave = gpio_pin_get_dt(&button1);
	
	printk("Start the Sen55\n");
	sen5x_init(DEVICE_DT_GET(DT_NODELABEL(i2c2)));
	sen5x_reset();
	
	printk("Get the sensor information\n");
	struct sen5x_info sen;
	sen5x_get_info(&sen);
	sen5x_print_info(&sen);
	
	struct lte_client_net_info net;
	lte_client_init_net_info(&net);
	//lte_client_get_net_info(&net);
	
	lora_client_auto_init();
	lora_client_set_client_id(0xFACE);
	lora_client_config(slave);
		
	struct sen5x_measurement pmm;
		
	if(slave){

		printk("loop\n");
		while (true) {
			printk("Start measurement\n");
			sen5x_start();

			printk("Sleep\n");
			//k_msleep(1*60000);
			k_msleep(10*1000);
			
			printk("Get measurement\n");
			sen5x_get_measurement(&pmm);
			sen5x_print_measurement(&pmm);
			
			printk("Send over LORA\n");
			lora_client_send((uint8_t*)&pmm, sizeof(pmm));
			
			printk("Stop measurement\n");
			sen5x_stop();
			
			printk("Sleep\n");
			//k_msleep(4*60000);
			k_msleep(10*1000);
		}
		
	}else{
		
		//printk("Listen to LORA\n");
		//lora_client_bind();
		struct lora_client_message msg;
		
		printk("Connect to the LTE\n");
		if(!lte_client_connect()){
			struct lte_client_system_mode mode;
			mode.lte_m=0; mode.nb_iot=1; mode.gnss=1; mode.lte_p=0;
			lte_client_set_system_mode(&mode);
			lte_client_connect();
		}
	
		printk("Get the network information\n");
		lte_client_get_net_info(&net);
		lte_client_print_net_info(&net);	
	
		printk("Get the gps coordinates\n");
		struct lte_client_gps_info gps;
		lte_client_init_gps_info(&gps);
		//lte_client_get_gps_info(&gps);
	
		
		
		printk("Loop\n");
		while (true) {

			//printk("Start measurement\n");
			//sen5x_start();

			//printk("Going to sleep\n");
			//k_msleep(1*60000);
			//k_msleep(10*1000);

			char payload[300];
			
			//sen5x_get_measurement(&pmm);
			//sen5x_print_measurement(&pmm);
			//build_request(payload,&pmm,&net,&gps);
			//printk("payload: %s\n",payload);
			//lte_client_post("https","sacaqm.web.cern.ch","443","/dbwrite.php",payload);
			
			lora_client_listen(30*1000);
			lora_client_print_buffer();
			
			while(lora_client_read(&msg)){
				printk("build request for received package");
				sen5x_parse_measurement(&pmm,msg.payload,msg.length);
				build_request(payload,&pmm,&net,&gps);
				printk("payload: %s\n",payload);
				lte_client_post("https","sacaqm.web.cern.ch","443","/dbwrite.php",payload);	
			}
	
			//printk("Stop measurement\n");
			//sen5x_stop();
	
			if(!gps.valid){
				//lte_client_get_gps_info(&gps);
			}else{
				printk("Sleep\n");
				//k_msleep(4*60000);
				k_msleep(10*1000);
			}
		}
	}

}

	